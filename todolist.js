	onload = todoListMain;

	function todoListMain(){
  		let Item,Category,Add;
		
		getElements();
		addListeners();
		
		
		function getElements(){
			Item = document.getElementsByTagName("input")[0];
			Category = document.getElementsByTagName("input")[1];
			Add = document.getElementById("field03");
		}
		
		function addListeners(){
			Add.addEventListener("click", addEntryItems, false);
		}

		
		function addEntryItems(event){
			let tab,Row,Column1,Column2,Column3,Column4,check,del,dall;
			
			if (Item.value === "" || Category.value === ""){
				alert("Item & Category cannot be left Empty")
			}
			else{
				tab = document.getElementById("toDoList");
				
				Row = document.createElement("tr");
				Row.className= "r";
				tab.appendChild(Row);
				
				check = document.createElement("input");
				check.type = "checkbox";
				check.addEventListener("click", checked, false);
				Column1 = document.createElement("td");
				Column1.align ="center";
				Column1.appendChild(check);
				Row.appendChild(Column1);
				
				Column2 = document.createElement("td");
				Column2.innerText = Item.value;
				Row.appendChild(Column2);
				
				Column3 = document.createElement("td");
				Column3.innerText = Category.value;
				Row.appendChild(Column3);
				
				del = document.createElement("span");
				del.innerText = "X";
				del.className = "material-icons";
				del.addEventListener("click", delRow, false);
				Column4 = document.createElement("td");
				Column4.className ="icons";
				Column4.appendChild(del);
				Row.appendChild(Column4);
				
				function delRow(){
					Row.remove();
				}
				
				function checked(){
					Row.classList.toggle("strike");
				}
				
				Item.value = "";
				Category.value = "";
				
				dall=document.getElementById("deleteAll");
				dall.addEventListener("click", deall, false);
				
				function deall(){
					tab.remove();
										
					let bo,tab2,tr1,th1,th2,th3,th4;
					
					bo=document.getElementsByTagName("div")[1];
					
					tab2 = document.createElement("table");
					tab2.className = "List";
					tab2.width = "100%";
					tab2.id = "toDoList";
					tab2.cellPadding = "5";
					bo.appendChild(tab2);
					
					tr1 = document.createElement("tr");
					tab2.appendChild(tr1);
					
					th1 = document.createElement("th");
					th1.width = "6%";
					tr1.appendChild(th1);
					
					th2 = document.createElement("th");
					th2.width = "64%";
					tr1.appendChild(th2);
					
					th3 = document.createElement("th");
					th3.width = "15%";
					tr1.appendChild(th3);
					
					th4 = document.createElement("th");
					th4.width = "15%";
					tr1.appendChild(th4);

				}
				
			}
			
		}
	}
